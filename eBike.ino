#include <MsTimer2.h>
#include <Servo.h>

#define TEMP  A0
#define HALL 2
#define THROTTLE A2
#define BRAKE A3
#define ESC 3

Servo esc;

const float wheelCirc = 91.1061869541;

boolean wasOnMag = 0;
unsigned long lastTrig = 0;
unsigned long trigInterval = 100000;

unsigned long lastTempRead = 0;
unsigned long lastTempUpdate = 0;
const int TEMPS_SIZE = 9;
int temps[TEMPS_SIZE] = {};

const int thrMin = 190;
const int thrMax = 870;
const int brakeMin = 210;
const int brakeMax = 867;
const int escMin = 700;
const int escMax = 2000;
const int escMid = (escMax + escMin) / 2;
const int escHalf = (escMax - escMin) / 2;
float throttle = 0;
float brake = 0;

float lastSpeed = 0;

void setup() {
  Serial.begin(9600);
  MsTimer2::set(1, readHall);
  MsTimer2::start();

  pinMode(ESC, OUTPUT);
  pinMode(HALL, INPUT);

  esc.attach(ESC);
}

void loop() {
  float newSpeed = wheelCirc / trigInterval * 56.8182f;
  float speed = abs(newSpeed - lastSpeed) > 10 ? lastSpeed : newSpeed;
  lastSpeed = speed;

  readThrottle();
  readBrake();

  unsigned long current = millis();
  if(current - lastTempRead >= 500) {
    readTemp();
    lastTempRead = current;
  }
  if(current - lastTempUpdate >= 5000) {
    lastTempUpdate = current;
    int temp = temps[TEMPS_SIZE / 2];
    setVal("temp.txt", "\"" + String(temp) + "\"");

  }

  setVal("speed.txt", "\"" + String(round(speed)) + "\"");

  float power = brake > 0 ? -1 * brake / 100.0f : throttle / 100.0f;

  esc.writeMicroseconds(escMid + round(escHalf * power));

  setVal("thrSlider.val", String(round(throttle)));
  setVal("throttle.txt", "\"" + String(round(throttle)) + "%\"");

  setVal("brakeSlider.val", String(round(brake)));
  setVal("brake.txt", "\"" + String(round(brake)) + "%\"");
}

float readThrottle() {
  int reading = analogRead(THROTTLE);
  throttle = (reading - thrMin + 0.0f) / (thrMax - thrMin);
  throttle = round(throttle * 10000.0f) / 100.0f;
  throttle = max(min(throttle, 100), 0);
}

float readBrake() {
  int reading = analogRead(BRAKE);
  brake = (reading - brakeMin + 0.0f) / (brakeMax - brakeMin);
  brake = round(brake * 10000.0f) / 100.0f;
  brake = max(min(brake, 100), 0);
}

void readTemp() {
  float vcc = readVcc() / 1000.0;
  float voltage = (analogRead(A0) / 1024.0) * vcc;
  float tempC = (voltage - .5) / .01;
  float tempF = tempC * 9 / 5 + 32;
  
  int tempTemps[TEMPS_SIZE] = {};
  
  for(int i = 1; i < TEMPS_SIZE; i++) {
    tempTemps[i] = temps[i - 1];
  }
  tempTemps[0] = round(tempF);
  for(int i = 0; i < TEMPS_SIZE; i++) {
    temps[i] = tempTemps[i];
  }

  for(int i = 0; i < TEMPS_SIZE; i++) {
    int smallest = 0;
    int smallestPos = 0;
    for(int i = i; i < TEMPS_SIZE; i++) {
      if(temps[i] < smallest) {
        smallest = temps[i];
        smallestPos = 0;
      }
    }
    int temp = temps[i];
    temps[i] = temps[smallestPos];
    temps[smallestPos] = temp;
  }
}

long readVcc() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH << 8;
  result = 1125300L / result; // Back-calculate AVcc in mV
  return result;
}

void readHall() {
  int reading = digitalRead(HALL);
  if (reading == LOW) {
    wasOnMag = true;
  } else if (wasOnMag) {
    wasOnMag = false;
    unsigned long current = millis();
    trigInterval = current - lastTrig;
    lastTrig = current;
  }

  if(millis() - lastTrig > 2600) {
    trigInterval = 20000;
  }
}
void setVal(String attribute, String val) {
  Serial.print(attribute + "=" + val);
  for(int i = 0; i < 3; i++)
    Serial.write(0xff);
}

